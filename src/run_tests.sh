#!/bin/bash
#ENTIRE FILE: python lsh.py -f ../data/NA19240_HiSeq_100_chr21.bam -k 12 -r 40 -b 25 -t lc_substring -s 0.95 -n 13528800

#Activate to print commands run by the script
#set -x

DATA_FILE="../data/NA19240_HiSeq_100_chr21.bam"
SIMILARITY_FUNCTIONS="lc_substring" # lc_subsequence" # others: jaccard
OUTPUT_DIR="test"
OUTPUT_LOG="test.log"
OUTPUT_LOG_ALL="all.log"
SIMILARITIES="0.9 0.5"
N_VALUES="10000 20000 50000 100000 200000 400000"


if [ -d ${OUTPUT_DIR} ]; then
    echo "Remove the directory \"${OUTPUT_DIR}\" before trying again!"
    exit 1
fi

if [ ! -d ${OUTPUT_DIR} ]; then
    mkdir ${OUTPUT_DIR}
fi

#Print header
PRINT_FORMAT="%-28s %-5s %-5s %-5s %-7s %-15s %-9s %40s\n"
echo -e "f\tk\tr\tb\tt\ts\tn\tseconds" > ${OUTPUT_DIR}/${OUTPUT_LOG}
printf "$PRINT_FORMAT" 'f' 'k' 'r' 'b' 's' 't' 'n' 'seconds'

#Run tests
for K in {4..10}
do
    for N in $N_VALUES
    do
        for S in $SIMILARITY_FUNCTIONS
        do
            for SIM in $SIMILARITIES
            do
                for R in {40..50..10}
                do
                    if [ "$R" == "50" ]; then
                        B=4
                    fi
                    if [ "$R" == "40" ]; then
                        B=5
                    fi

                    TIME_START=`date +%s%3N`
                    python lsh.py -f ${DATA_FILE} -k ${K} -r ${R} -b ${B} -t ${S} -s ${SIM} -n ${N} >> ${OUTPUT_DIR}/${OUTPUT_LOG_ALL}
                    #python -m cProfile -o ${OUTPUT_DIR}/profile.pstats lsh.py -f ${DATA_FILE} -k ${K} -r ${R} -b ${B} -t ${S} -s ${SIM} -n ${N} > ${OUTPUT_DIR}/${OUTPUT_LOG_ALL}
                    if [ $? -ne 0 ]; then
                        # If exit code of python script is anything else than success, then stop running tests
                        exit $?
                    fi
                    TIME_STOP=`date +%s%3N`

                    MILLISECONDS_ELAPSED=$((TIME_STOP - TIME_START))
                    SECONDS_ELAPSED=`bc -l <<< ${MILLISECONDS_ELAPSED}/1000`

                    #Print parameters with runtime in seconds
                    echo -e "$(basename $DATA_FILE)\t${K}\t${R}\t${B}\t${S}\t${SIM}\t${N}\t${SECONDS_ELAPSED}" >> ${OUTPUT_DIR}/${OUTPUT_LOG}
                    printf "$PRINT_FORMAT" $(basename $DATA_FILE) ${K} ${R} ${B} ${SIM} ${S} ${N} ${SECONDS_ELAPSED}
#                    gprof2dot -f pstats ${OUTPUT_DIR}/profile.pstats | dot -Tsvg -o ${OUTPUT_DIR}/profile_f$(basename $DATA_FILE)_n${N}_t${S}_k${K}_r${R}_b${B}_s${SIM}.svg
                done
            done
        done
    done
done
