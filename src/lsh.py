#!/usr/local/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import argparse
from collections import defaultdict
import datetime
import pysam
import random
import sys
import itertools

all_reads = list()
decoded_shingles = {}


def timeprint(string):
    print('%s | %s' % ((datetime.datetime.now()-global_start), string))
    return


def get_shingles(read, size):
    for i in xrange(len(read)-size+1):
        yield read[i:i+size]


# http://wordaligned.org/articles/longest-common-subsequence#tocreducing-the-space-for-lcs-lengths
def longest_common_subsequence_length(xs, ys):
    ny = len(ys)
    curr = list(itertools.repeat(0, 1 + ny))
    for x in xs:
        prev = list(curr)
        for i, y in enumerate(ys):
            if x == y:
                curr[i+1] = prev[i] + 1
            else:
                curr[i+1] = max(curr[i], prev[i+1])
    return curr[ny]


# https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring#Python2
def longest_common_substring_length(s1, s2):
    m = [[0] * (1 + len(s2)) for _ in xrange(1 + len(s1))]
    longest = 0
    for x in xrange(1, 1 + len(s1)):
        for y in xrange(1, 1 + len(s2)):
            if s1[x - 1] == s2[y - 1]:
                m[x][y] = m[x - 1][y - 1] + 1
                if m[x][y] > longest:
                    longest = m[x][y]
            else:
                m[x][y] = 0
    return longest


def get_sim_subsequence(s1, s2):
    lcs_length = longest_common_subsequence_length(s1, s2)
    longest_read = max(len(s1), len(s2))
    return lcs_length / float(longest_read)


def get_sim_substring(s1, s2):
    lcs_length = longest_common_substring_length(s1, s2)
    longest_read = max(len(s1), len(s2))
    return lcs_length / float(longest_read)


def get_sim_jaccard(s1, s2):
    x = len(s1.intersection(s2))
    y = len(s1.union(s2))
    return x / float(y)


def decode_shingle(shingle_size, shingle_number):
    if shingle_number in decoded_shingles:
        return decoded_shingles[shingle_number]

    result = []
    temp = shingle_number
    while temp != 0:
        if temp % 4 == 0:
            result.append('A')
        elif temp % 4 == 1:
            result.append('C')
        elif temp % 4 == 2:
            result.append('G')
        elif temp % 4 == 3:
            result.append('T')
        temp >>= 2
    result.extend(['A'] * (shingle_size-len(result)))
    decoded_shingles[shingle_number] = ''.join(reversed(result))
    return decoded_shingles[shingle_number]


def shuffle_copy(data, i):
    random.seed(i)
    copy = data[:]
    random.shuffle(copy)
    return copy


def elapsed_since(from_time):
    elapsed = datetime.datetime.now() - from_time
    return elapsed.seconds + elapsed.microseconds / 1e6


def check_candidates_jaccard(pairs, k, s, o):
    with open(o, 'w') as output_file:
        report_count = 0
        num_pairs = len(pairs)

        timeprint('Status: Checking %i candidate pairs using Jaccard.'
                  % num_pairs)
        for read1, read2 in pairs:
            shingles_read1 = set(get_shingles(read=all_reads[read1], size=k))
            shingles_read2 = set(get_shingles(read=all_reads[read2], size=k))
            real_similarity = get_sim_jaccard(shingles_read1,
                                              shingles_read2)
            if real_similarity >= s:
                report_count += 1
                output_file.write('%f %s %s\n' % (real_similarity,
                                                  all_reads[read1],
                                                  all_reads[read2]))
        timeprint('Candidate pairs that are above threshold: %i / %i.'
                  % (report_count, num_pairs))
    return report_count


def check_candidates(similarity_func, pairs, s, o):
    with open(o, 'w') as output_file:
        report_count = 0
        num_pairs = len(pairs)
        timeprint('Status: Checking %i candidate pairs.' % num_pairs)
        for read1, read2 in pairs:
            string_read1 = all_reads[read1]
            string_read2 = all_reads[read2]
            real_similarity = similarity_func(string_read1, string_read2)
            if real_similarity >= s:
                report_count += 1
                output_file.write('%f %s %s\n' % (real_similarity,
                                                  string_read1,
                                                  string_read2))
        timeprint('Candidate pairs that are above threshold: %i / %i.'
                  % (report_count, num_pairs))

    return report_count


def process_bands(__sig_matrix, b, r):
    timeprint('Processing bands for candidate pairs.')
    __candidate_pairs = set()

    for band in xrange(b):
        buckets = defaultdict(list)

        # Iterate through signature matrix and insert row/read numbers number
        # in the buckets that they hash to for the given band.
        for idx, current_sig in enumerate(__sig_matrix):
            band_string = ""
            for i in xrange(r):
                band_string += str(current_sig[band*r+i])
            if band_string not in buckets:
                buckets[band_string] = []
            buckets[band_string].append(idx)

        # Now that we have all buckets containing the row/read numbers,
        # we need to generate candidate pairs.
        for bucket in buckets:
            bucket_size = len(buckets[bucket])
            if bucket_size > 1:
                for i in xrange(0, bucket_size):
                    for j in xrange(i+1, bucket_size):
                        __candidate_pairs.add((buckets[bucket][i],
                                              buckets[bucket][j]))

    return __candidate_pairs


def read_bam_file_final(f, k, r, b, n):
    global start_time
    timeprint('Generating permutations.')

    reads_scanned = 0
    __sig_matrix = []
    sam_file = pysam.Samfile(f, 'rb')
    possibilities = ['A', 'C', 'G', 'T']

    permutations = defaultdict()
    for perm_no in xrange(r*b):
        init_numbers = list(xrange(pow(4, k)))
        new_permutation = shuffle_copy(init_numbers, perm_no)
        for ival, element in enumerate(new_permutation):
            shingle = decode_shingle(shingle_size=k, shingle_number=ival)
            if shingle not in permutations:
                permutations[shingle] = []
            permutations[shingle].append(element)

    timeprint('Reading input file and creating signature matrix.')
    start_time = datetime.datetime.now()
    for aligned_read in sam_file.fetch(until_eof=True):
        cur_read = aligned_read.seq
        if 'N' in cur_read:
            new_string = ''
            for c in cur_read:
                if c == 'N':
                    new_string += random.choice(possibilities)
                else:
                    new_string += c
            cur_read = new_string
        cur_shingles = set(get_shingles(read=cur_read, size=k))

        cur_signature = []
        for perm_no in xrange(r*b):
            # Set initial to maxint
            min_hash = sys.maxint
            # Run through current shingles
            for shingle in cur_shingles:
                # If placement in current permutation is lower than previous,
                # update min_hash and shingleNumber
                if permutations[shingle][perm_no] < min_hash:
                    min_hash = permutations[shingle][perm_no]
            if min_hash == sys.maxint:
                sys.stderr.write('FATAL: Did not find any hash value.\n')
                exit(1)
            cur_signature.append(min_hash)

        __sig_matrix.append(cur_signature)

        reads_scanned += 1
        all_reads.append(cur_read)
        if reads_scanned >= n:
            break

    sam_file.close()
    return __sig_matrix


def read_bam_file_initial(f, k, n):
    timeprint('Reading input file and creating signature matrix.')

    reads_scanned = 0
    __sig_matrix = []
    sam_file = pysam.Samfile(f, 'rb')
    possibilities = ['A', 'C', 'G', 'T']

    permutations = generate_permutations(args.b * args.r, args.k)

    for alignedread in sam_file.fetch(until_eof=True):
        cur_signature = []
        cur_read = alignedread.seq
        if 'N' in cur_read:
            new_string = ''
            for c in cur_read:
                if c == 'N':
                    new_string += random.choice(possibilities)
                else:
                    new_string += c
            cur_read = new_string
        cur_shingles = set(get_shingles(read=cur_read, size=k))

        for perm in permutations:
            for ival in perm:
                if decode_shingle(shingle_size=args.k,
                                  shingle_number=ival) in cur_shingles:
                    cur_signature.append(ival)
                    break

        __sig_matrix.append(cur_signature)

        reads_scanned += 1
        all_reads.append(cur_read)
        if reads_scanned >= n:
            break

    sam_file.close()
    return __sig_matrix


def generate_permutations(count, k):
    init_numbers = list(xrange(pow(4, k)))
    permutations = [shuffle_copy(init_numbers, i) for i in xrange(count)]
    return permutations


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f',
                        type=str,
                        required=True,
                        help='path to input file')
    parser.add_argument('-o',
                        type=str,
                        required=True,
                        help='path to store output containing similar reads')
    parser.add_argument('-k',
                        type=int,
                        default=4,
                        help='length of shingles')
    parser.add_argument('-r',
                        type=int,
                        default=50,
                        help='number of rows per band')
    parser.add_argument('-b',
                        type=int,
                        default=2,
                        help='number of bands')
    parser.add_argument('-s',
                        type=float,
                        help='similarity threshold for reported read pairs')
    parser.add_argument('-t',
                        type=str,
                        default='lc_substring',
                        help='type of similarity measure.',
                        choices=['lc_substring', 'lc_subsequence', 'jaccard'])
    parser.add_argument('-n',
                        type=int,
                        default=sys.maxint,
                        help='number of sequences to read from input file')
    parser.add_argument('-l',
                        type=str,
                        help='log file of execution')
    args = parser.parse_args()
    global_start = datetime.datetime.now()
    sig_matrix = 0

    if args.l is not None:
        sys.stdout = open(args.l, 'w')

    if not args.s:
        print('Using:\n\tk = %i\n\tr = %i\n\tb = %i\n\tn = %i\n'
              % (args.k, args.r, args.b, args.n))
        args.s = 0
    else:
        print('Using:\n\tk = %i\n\tr = %i\n\tb = %i\n\tn = %i\n'
              '\tt = %s\n\ts = %f\n'
              % (args.k, args.r, args.b, args.n, args.t, args.s))

    file_extension = args.f.split('.')[-1:][0]
    start_time = datetime.datetime.now()
    if file_extension == 'bam':
        sig_matrix = read_bam_file_final(f=args.f, k=args.k, r=args.r,
                                          b=args.b, n=args.n)
        # sig_matrix = read_bam_file_initial(f=args.f, k=args.k, n=args.n)
    else:
        sys.stderr.write('ERROR: File format not supported: %s\n'
                         % file_extension)
        exit(1)
    time_gen_sig_matrix = elapsed_since(from_time=start_time)

    start_time = datetime.datetime.now()
    candidate_pairs = process_bands(__sig_matrix=sig_matrix,
                                    b=args.b, r=args.r)
    time_processing_bands = elapsed_since(from_time=start_time)

    start_time = datetime.datetime.now()
    rep_count = 0
    if args.t == 'lc_substring':
        rep_count = check_candidates(similarity_func=get_sim_substring,
                                     pairs=candidate_pairs, s=args.s, o=args.o)
    elif args.t == 'lc_subsequence':
        rep_count = check_candidates(similarity_func=get_sim_subsequence,
                                     pairs=candidate_pairs, s=args.s, o=args.o)
    elif args.t == 'jaccard':
        rep_count = check_candidates_jaccard(pairs=candidate_pairs, k=args.k,
                                             s=args.s, o=args.o)
    time_checking_candidates = elapsed_since(from_time=start_time)

    print('\t'.join(['DATA', 'r', 'b', 'k', 'n', 's', 'time(scan/sig)',
                     'time(find_candidates)', 'time(check_candidates)',
                     'candidate_count', 'report_count']))
    print('\t'.join(['DATA', str(args.r), str(args.b), str(args.k),
          str(args.n), str(args.s), str(time_gen_sig_matrix),
          str(time_processing_bands),
          str(time_checking_candidates), str(len(candidate_pairs)),
          str(rep_count)]))

    if args.l:
        sys.stdout.close()
